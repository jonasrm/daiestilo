//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DaiEstilo.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Transacoes
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Transacoes()
        {
            this.TransacoesDetalhe = new HashSet<TransacoesDetalhe>();
        }
    
        public int IdTransacao { get; set; }
        public string Transacao_ID { get; set; }
        public string Cliente_Nome { get; set; }
        public string Cliente_Email { get; set; }
        public string Debito_Credito { get; set; }
        public string Tipo_Transacao { get; set; }
        public string Status { get; set; }
        public string Tipo_Pagamento { get; set; }
        public decimal Valor_Bruto { get; set; }
        public Nullable<decimal> Valor_Desconto { get; set; }
        public Nullable<decimal> Valor_Taxa { get; set; }
        public decimal Valor_Liquido { get; set; }
        public string Transportadora { get; set; }
        public string Num_Envio { get; set; }
        public System.DateTime Data_Transacao { get; set; }
        public Nullable<System.DateTime> Data_Compensacao { get; set; }
        public string Ref_Transacao { get; set; }
        public Nullable<int> Parcelas { get; set; }
        public Nullable<int> Codigo_Usuario { get; set; }
        public Nullable<int> Codigo_Venda { get; set; }
        public Nullable<int> Serial_Leitor { get; set; }
        public Nullable<int> Recebimentos { get; set; }
        public Nullable<int> Recebidos { get; set; }
        public Nullable<decimal> Valor_Recebido { get; set; }
        public Nullable<decimal> Valor_Tarifa_Intermediacao { get; set; }
        public Nullable<decimal> Valor_Taxa_Intermediacao { get; set; }
        public Nullable<decimal> Valor_Taxa_Parcelamento { get; set; }
        public Nullable<decimal> Valor_Tarifa_Boleto { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TransacoesDetalhe> TransacoesDetalhe { get; set; }
    }
}
