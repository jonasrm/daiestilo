﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DaiEstilo.Classes
{
    public class Util
    {

        public const string ComboVazia = "Selecione...";
        public const string ComboTodos = "Todos";

        public static void RetornarMascara(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;

            if (string.IsNullOrEmpty(txt.Text))
                txt.Text = "0";

            txt.Text = double.Parse(txt.Text).ToString("C2");
        }

        public static void TirarMascara(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            txt.Text = txt.Text.Replace("R$", "").Trim();
        }

        public static void ApenasValorNumericoVirgula(object sender, KeyPressEventArgs e)
        {
            TextBox txt = (TextBox)sender;
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != Convert.ToChar(Keys.Back))
            {
                if (e.KeyChar == ',')
                    e.Handled = (txt.Text.Contains(','));
                else
                    e.Handled = true;
            }
        }

        public static void ApenasValorNumerico(object sender, KeyPressEventArgs e)
        {
            TextBox txt = (TextBox)sender;
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != Convert.ToChar(Keys.Back))
            {
                e.Handled = true;
            }
        }

        public static void AplicarEventosMoeda(TextBox txt)
        {
            txt.Enter += TirarMascara;
            txt.Leave += RetornarMascara;
            txt.KeyPress += ApenasValorNumericoVirgula;
        }

        public static void AplicarEventosInteiro(TextBox txt)
        {
            txt.KeyPress += ApenasValorNumerico;
        }

    }

}
