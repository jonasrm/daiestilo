﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DaiEstilo.Classes
{
    public class ItemData
    {
        public int key { get; set; }
        public string value { get; set; }

        public ItemData(int key, string value)
        {
            this.key = key;
            this.value = value;
        }
    }
}
