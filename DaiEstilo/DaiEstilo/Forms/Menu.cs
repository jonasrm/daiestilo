﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DaiEstilo.Forms
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var frm = new ImportacaoArquivoTransacao();
            frm.ShowDialog(this);
        }

        private void btnRelatorioVendas_Click(object sender, EventArgs e)
        {
            var frm = new GerenciaVendas();
            frm.ShowDialog(this);
        }
    }
}
