﻿namespace DaiEstilo.Forms
{
    partial class DetalhamentoVenda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvDetalhamentoVenda = new System.Windows.Forms.DataGridView();
            this.dgvDetalhes = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtObservacao = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.txtValor = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAdicionarPeca = new System.Windows.Forms.Button();
            this.cmbPeca = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAdicionarLoja = new System.Windows.Forms.Button();
            this.cmbLoja = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.DeleteRow = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.excluirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalhamentoVenda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalhes)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.DeleteRow.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvDetalhamentoVenda
            // 
            this.dgvDetalhamentoVenda.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvDetalhamentoVenda.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvDetalhamentoVenda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetalhamentoVenda.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvDetalhamentoVenda.Location = new System.Drawing.Point(12, 12);
            this.dgvDetalhamentoVenda.MultiSelect = false;
            this.dgvDetalhamentoVenda.Name = "dgvDetalhamentoVenda";
            this.dgvDetalhamentoVenda.ReadOnly = true;
            this.dgvDetalhamentoVenda.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDetalhamentoVenda.Size = new System.Drawing.Size(711, 80);
            this.dgvDetalhamentoVenda.TabIndex = 0;
            // 
            // dgvDetalhes
            // 
            this.dgvDetalhes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetalhes.ContextMenuStrip = this.DeleteRow;
            this.dgvDetalhes.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvDetalhes.Location = new System.Drawing.Point(12, 303);
            this.dgvDetalhes.Name = "dgvDetalhes";
            this.dgvDetalhes.ReadOnly = true;
            this.dgvDetalhes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDetalhes.Size = new System.Drawing.Size(708, 159);
            this.dgvDetalhes.TabIndex = 8;
            this.dgvDetalhes.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgvDetalhes_MouseDown);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtObservacao);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btnSalvar);
            this.groupBox1.Controls.Add(this.txtValor);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnAdicionarPeca);
            this.groupBox1.Controls.Add(this.cmbPeca);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnAdicionarLoja);
            this.groupBox1.Controls.Add(this.cmbLoja);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(12, 98);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(711, 193);
            this.groupBox1.TabIndex = 38;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dados dos Itens";
            // 
            // txtObservacao
            // 
            this.txtObservacao.Location = new System.Drawing.Point(79, 119);
            this.txtObservacao.Multiline = true;
            this.txtObservacao.Name = "txtObservacao";
            this.txtObservacao.Size = new System.Drawing.Size(522, 59);
            this.txtObservacao.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 17);
            this.label4.TabIndex = 48;
            this.label4.Text = "Obs.:";
            // 
            // btnSalvar
            // 
            this.btnSalvar.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.Location = new System.Drawing.Point(607, 89);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(86, 89);
            this.btnSalvar.TabIndex = 7;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // txtValor
            // 
            this.txtValor.Location = new System.Drawing.Point(79, 89);
            this.txtValor.Name = "txtValor";
            this.txtValor.Size = new System.Drawing.Size(126, 24);
            this.txtValor.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 17);
            this.label2.TabIndex = 44;
            this.label2.Text = "Valor:";
            // 
            // btnAdicionarPeca
            // 
            this.btnAdicionarPeca.Location = new System.Drawing.Point(79, 58);
            this.btnAdicionarPeca.Name = "btnAdicionarPeca";
            this.btnAdicionarPeca.Size = new System.Drawing.Size(126, 27);
            this.btnAdicionarPeca.TabIndex = 3;
            this.btnAdicionarPeca.Text = "Adicionar Novo...";
            this.btnAdicionarPeca.UseVisualStyleBackColor = true;
            this.btnAdicionarPeca.Click += new System.EventHandler(this.btnAdicionarPeca_Click);
            // 
            // cmbPeca
            // 
            this.cmbPeca.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPeca.FormattingEnabled = true;
            this.cmbPeca.Location = new System.Drawing.Point(211, 60);
            this.cmbPeca.Name = "cmbPeca";
            this.cmbPeca.Size = new System.Drawing.Size(482, 23);
            this.cmbPeca.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 17);
            this.label1.TabIndex = 42;
            this.label1.Text = "Peça:";
            // 
            // btnAdicionarLoja
            // 
            this.btnAdicionarLoja.Location = new System.Drawing.Point(79, 29);
            this.btnAdicionarLoja.Name = "btnAdicionarLoja";
            this.btnAdicionarLoja.Size = new System.Drawing.Size(126, 27);
            this.btnAdicionarLoja.TabIndex = 1;
            this.btnAdicionarLoja.Text = "Adicionar Novo...";
            this.btnAdicionarLoja.UseVisualStyleBackColor = true;
            this.btnAdicionarLoja.Click += new System.EventHandler(this.btnAdicionarLoja_Click);
            // 
            // cmbLoja
            // 
            this.cmbLoja.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLoja.FormattingEnabled = true;
            this.cmbLoja.Location = new System.Drawing.Point(211, 31);
            this.cmbLoja.Name = "cmbLoja";
            this.cmbLoja.Size = new System.Drawing.Size(482, 23);
            this.cmbLoja.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 17);
            this.label3.TabIndex = 39;
            this.label3.Text = "Loja:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 475);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 17);
            this.label5.TabIndex = 40;
            this.label5.Text = "Total:";
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.lblTotal.Location = new System.Drawing.Point(58, 475);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(33, 17);
            this.lblTotal.TabIndex = 41;
            this.lblTotal.Text = "0,00";
            // 
            // DeleteRow
            // 
            this.DeleteRow.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.excluirToolStripMenuItem});
            this.DeleteRow.Name = "DeleteRow";
            this.DeleteRow.Size = new System.Drawing.Size(109, 26);
            this.DeleteRow.Click += new System.EventHandler(this.DeleteRow_Click);
            // 
            // excluirToolStripMenuItem
            // 
            this.excluirToolStripMenuItem.Name = "excluirToolStripMenuItem";
            this.excluirToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.excluirToolStripMenuItem.Text = "Excluir";
            // 
            // DetalhamentoVenda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(735, 501);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgvDetalhes);
            this.Controls.Add(this.dgvDetalhamentoVenda);
            this.Font = new System.Drawing.Font("Calibri", 10F);
            this.Name = "DetalhamentoVenda";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Detalhamento da Venda";
            this.Load += new System.EventHandler(this.DetalhamentoVenda_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalhamentoVenda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalhes)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.DeleteRow.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvDetalhamentoVenda;
        private System.Windows.Forms.DataGridView dgvDetalhes;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtValor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAdicionarPeca;
        private System.Windows.Forms.ComboBox cmbPeca;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAdicionarLoja;
        private System.Windows.Forms.ComboBox cmbLoja;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.TextBox txtObservacao;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.ContextMenuStrip DeleteRow;
        private System.Windows.Forms.ToolStripMenuItem excluirToolStripMenuItem;
    }
}