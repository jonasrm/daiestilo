﻿namespace DaiEstilo.Forms
{
    partial class GerenciaVendas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.txtQuantidadeParcelas = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtValorVendaFinal = new System.Windows.Forms.TextBox();
            this.txtValorVendaInicial = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbOperacao = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbSituacao = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkDataCompensacao = new System.Windows.Forms.CheckBox();
            this.chkDataTransacao = new System.Windows.Forms.CheckBox();
            this.dtpDataCompensacaoFinal = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpDataCompensacaoInicial = new System.Windows.Forms.DateTimePicker();
            this.cmbTipoPagamento = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpDataTransacaoFinal = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpDataTransacaoInicial = new System.Windows.Forms.DateTimePicker();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.dgvVendas = new System.Windows.Forms.DataGridView();
            this.label9 = new System.Windows.Forms.Label();
            this.lblQuantidadeRegistros = new System.Windows.Forms.Label();
            this.lblTotalBruto = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblTotalLiquido = new System.Windows.Forms.Label();
            this.ccc = new System.Windows.Forms.Label();
            this.lblTotalTaxa = new System.Windows.Forms.Label();
            this.bbb = new System.Windows.Forms.Label();
            this.lblTotalPercentual = new System.Windows.Forms.Label();
            this.aaaa = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVendas)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnPesquisar);
            this.groupBox1.Controls.Add(this.txtQuantidadeParcelas);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtValorVendaFinal);
            this.groupBox1.Controls.Add(this.txtValorVendaInicial);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cmbOperacao);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.cmbSituacao);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.chkDataCompensacao);
            this.groupBox1.Controls.Add(this.chkDataTransacao);
            this.groupBox1.Controls.Add(this.dtpDataCompensacaoFinal);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.dtpDataCompensacaoInicial);
            this.groupBox1.Controls.Add(this.cmbTipoPagamento);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dtpDataTransacaoFinal);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dtpDataTransacaoInicial);
            this.groupBox1.Location = new System.Drawing.Point(14, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1456, 152);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtros";
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPesquisar.Location = new System.Drawing.Point(1056, 25);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(387, 112);
            this.btnPesquisar.TabIndex = 12;
            this.btnPesquisar.Text = "Pesquisar";
            this.btnPesquisar.UseVisualStyleBackColor = true;
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisa_Click);
            // 
            // txtQuantidadeParcelas
            // 
            this.txtQuantidadeParcelas.Location = new System.Drawing.Point(694, 82);
            this.txtQuantidadeParcelas.MaxLength = 3;
            this.txtQuantidadeParcelas.Name = "txtQuantidadeParcelas";
            this.txtQuantidadeParcelas.Size = new System.Drawing.Size(110, 24);
            this.txtQuantidadeParcelas.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(547, 85);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(128, 17);
            this.label8.TabIndex = 42;
            this.label8.Text = "Quantidade Parcelas:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(310, 114);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 17);
            this.label7.TabIndex = 41;
            this.label7.Text = "até";
            // 
            // txtValorVendaFinal
            // 
            this.txtValorVendaFinal.Location = new System.Drawing.Point(361, 111);
            this.txtValorVendaFinal.MaxLength = 10;
            this.txtValorVendaFinal.Name = "txtValorVendaFinal";
            this.txtValorVendaFinal.Size = new System.Drawing.Size(128, 24);
            this.txtValorVendaFinal.TabIndex = 6;
            // 
            // txtValorVendaInicial
            // 
            this.txtValorVendaInicial.Location = new System.Drawing.Point(161, 111);
            this.txtValorVendaInicial.MaxLength = 10;
            this.txtValorVendaInicial.Name = "txtValorVendaInicial";
            this.txtValorVendaInicial.Size = new System.Drawing.Size(128, 24);
            this.txtValorVendaInicial.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 114);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 17);
            this.label6.TabIndex = 38;
            this.label6.Text = "Valor Venda:";
            // 
            // cmbOperacao
            // 
            this.cmbOperacao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOperacao.FormattingEnabled = true;
            this.cmbOperacao.Location = new System.Drawing.Point(161, 82);
            this.cmbOperacao.Name = "cmbOperacao";
            this.cmbOperacao.Size = new System.Drawing.Size(328, 23);
            this.cmbOperacao.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 17);
            this.label5.TabIndex = 36;
            this.label5.Text = "Operação:";
            // 
            // cmbSituacao
            // 
            this.cmbSituacao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSituacao.FormattingEnabled = true;
            this.cmbSituacao.Location = new System.Drawing.Point(694, 53);
            this.cmbSituacao.Name = "cmbSituacao";
            this.cmbSituacao.Size = new System.Drawing.Size(328, 23);
            this.cmbSituacao.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(547, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 17);
            this.label1.TabIndex = 34;
            this.label1.Text = "Situação:";
            // 
            // chkDataCompensacao
            // 
            this.chkDataCompensacao.AutoSize = true;
            this.chkDataCompensacao.Location = new System.Drawing.Point(550, 24);
            this.chkDataCompensacao.Name = "chkDataCompensacao";
            this.chkDataCompensacao.Size = new System.Drawing.Size(140, 21);
            this.chkDataCompensacao.TabIndex = 7;
            this.chkDataCompensacao.Text = "Data Compensação:";
            this.chkDataCompensacao.UseVisualStyleBackColor = true;
            this.chkDataCompensacao.CheckedChanged += new System.EventHandler(this.chkDataCompensacao_CheckedChanged);
            // 
            // chkDataTransacao
            // 
            this.chkDataTransacao.AutoSize = true;
            this.chkDataTransacao.Location = new System.Drawing.Point(17, 25);
            this.chkDataTransacao.Name = "chkDataTransacao";
            this.chkDataTransacao.Size = new System.Drawing.Size(119, 21);
            this.chkDataTransacao.TabIndex = 0;
            this.chkDataTransacao.Text = "Data Transação:";
            this.chkDataTransacao.UseVisualStyleBackColor = true;
            this.chkDataTransacao.CheckedChanged += new System.EventHandler(this.chkDataTransacao_CheckedChanged);
            // 
            // dtpDataCompensacaoFinal
            // 
            this.dtpDataCompensacaoFinal.CustomFormat = "";
            this.dtpDataCompensacaoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDataCompensacaoFinal.Location = new System.Drawing.Point(894, 20);
            this.dtpDataCompensacaoFinal.MaxDate = new System.DateTime(2020, 12, 31, 0, 0, 0, 0);
            this.dtpDataCompensacaoFinal.MinDate = new System.DateTime(2015, 1, 1, 0, 0, 0, 0);
            this.dtpDataCompensacaoFinal.Name = "dtpDataCompensacaoFinal";
            this.dtpDataCompensacaoFinal.Size = new System.Drawing.Size(128, 24);
            this.dtpDataCompensacaoFinal.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(844, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 17);
            this.label4.TabIndex = 30;
            this.label4.Text = "até";
            // 
            // dtpDataCompensacaoInicial
            // 
            this.dtpDataCompensacaoInicial.CustomFormat = "";
            this.dtpDataCompensacaoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDataCompensacaoInicial.Location = new System.Drawing.Point(694, 20);
            this.dtpDataCompensacaoInicial.MaxDate = new System.DateTime(2020, 12, 31, 0, 0, 0, 0);
            this.dtpDataCompensacaoInicial.MinDate = new System.DateTime(2015, 1, 1, 0, 0, 0, 0);
            this.dtpDataCompensacaoInicial.Name = "dtpDataCompensacaoInicial";
            this.dtpDataCompensacaoInicial.Size = new System.Drawing.Size(128, 24);
            this.dtpDataCompensacaoInicial.TabIndex = 8;
            // 
            // cmbTipoPagamento
            // 
            this.cmbTipoPagamento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoPagamento.FormattingEnabled = true;
            this.cmbTipoPagamento.Location = new System.Drawing.Point(161, 53);
            this.cmbTipoPagamento.Name = "cmbTipoPagamento";
            this.cmbTipoPagamento.Size = new System.Drawing.Size(328, 23);
            this.cmbTipoPagamento.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 17);
            this.label3.TabIndex = 27;
            this.label3.Text = "Tipo Pagamento:";
            // 
            // dtpDataTransacaoFinal
            // 
            this.dtpDataTransacaoFinal.CustomFormat = "";
            this.dtpDataTransacaoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDataTransacaoFinal.Location = new System.Drawing.Point(361, 23);
            this.dtpDataTransacaoFinal.MaxDate = new System.DateTime(2020, 12, 31, 0, 0, 0, 0);
            this.dtpDataTransacaoFinal.MinDate = new System.DateTime(2015, 1, 1, 0, 0, 0, 0);
            this.dtpDataTransacaoFinal.Name = "dtpDataTransacaoFinal";
            this.dtpDataTransacaoFinal.Size = new System.Drawing.Size(128, 24);
            this.dtpDataTransacaoFinal.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(310, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 17);
            this.label2.TabIndex = 25;
            this.label2.Text = "até";
            // 
            // dtpDataTransacaoInicial
            // 
            this.dtpDataTransacaoInicial.CustomFormat = "";
            this.dtpDataTransacaoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDataTransacaoInicial.Location = new System.Drawing.Point(161, 23);
            this.dtpDataTransacaoInicial.MaxDate = new System.DateTime(2020, 12, 31, 0, 0, 0, 0);
            this.dtpDataTransacaoInicial.MinDate = new System.DateTime(2015, 1, 1, 0, 0, 0, 0);
            this.dtpDataTransacaoInicial.Name = "dtpDataTransacaoInicial";
            this.dtpDataTransacaoInicial.Size = new System.Drawing.Size(128, 24);
            this.dtpDataTransacaoInicial.TabIndex = 1;
            // 
            // dgvVendas
            // 
            this.dgvVendas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVendas.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvVendas.Location = new System.Drawing.Point(14, 172);
            this.dgvVendas.Name = "dgvVendas";
            this.dgvVendas.ReadOnly = true;
            this.dgvVendas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVendas.Size = new System.Drawing.Size(1456, 424);
            this.dgvVendas.TabIndex = 45;
            this.dgvVendas.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVendas_CellDoubleClick);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 606);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 17);
            this.label9.TabIndex = 43;
            this.label9.Text = "Registros:";
            // 
            // lblQuantidadeRegistros
            // 
            this.lblQuantidadeRegistros.AutoSize = true;
            this.lblQuantidadeRegistros.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantidadeRegistros.Location = new System.Drawing.Point(81, 607);
            this.lblQuantidadeRegistros.Name = "lblQuantidadeRegistros";
            this.lblQuantidadeRegistros.Size = new System.Drawing.Size(14, 15);
            this.lblQuantidadeRegistros.TabIndex = 46;
            this.lblQuantidadeRegistros.Text = "0";
            // 
            // lblTotalBruto
            // 
            this.lblTotalBruto.AutoSize = true;
            this.lblTotalBruto.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalBruto.Location = new System.Drawing.Point(281, 607);
            this.lblTotalBruto.Name = "lblTotalBruto";
            this.lblTotalBruto.Size = new System.Drawing.Size(14, 15);
            this.lblTotalBruto.TabIndex = 48;
            this.lblTotalBruto.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(200, 606);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 17);
            this.label11.TabIndex = 47;
            this.label11.Text = "Total Bruto:";
            // 
            // lblTotalLiquido
            // 
            this.lblTotalLiquido.AutoSize = true;
            this.lblTotalLiquido.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalLiquido.Location = new System.Drawing.Point(497, 607);
            this.lblTotalLiquido.Name = "lblTotalLiquido";
            this.lblTotalLiquido.Size = new System.Drawing.Size(14, 15);
            this.lblTotalLiquido.TabIndex = 50;
            this.lblTotalLiquido.Text = "0";
            // 
            // ccc
            // 
            this.ccc.AutoSize = true;
            this.ccc.Location = new System.Drawing.Point(408, 606);
            this.ccc.Name = "ccc";
            this.ccc.Size = new System.Drawing.Size(83, 17);
            this.ccc.TabIndex = 49;
            this.ccc.Text = "Total Líquido:";
            // 
            // lblTotalTaxa
            // 
            this.lblTotalTaxa.AutoSize = true;
            this.lblTotalTaxa.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalTaxa.Location = new System.Drawing.Point(691, 607);
            this.lblTotalTaxa.Name = "lblTotalTaxa";
            this.lblTotalTaxa.Size = new System.Drawing.Size(14, 15);
            this.lblTotalTaxa.TabIndex = 52;
            this.lblTotalTaxa.Text = "0";
            // 
            // bbb
            // 
            this.bbb.AutoSize = true;
            this.bbb.Location = new System.Drawing.Point(616, 606);
            this.bbb.Name = "bbb";
            this.bbb.Size = new System.Drawing.Size(69, 17);
            this.bbb.TabIndex = 51;
            this.bbb.Text = "Total Taxa:";
            // 
            // lblTotalPercentual
            // 
            this.lblTotalPercentual.AutoSize = true;
            this.lblTotalPercentual.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalPercentual.Location = new System.Drawing.Point(880, 607);
            this.lblTotalPercentual.Name = "lblTotalPercentual";
            this.lblTotalPercentual.Size = new System.Drawing.Size(14, 15);
            this.lblTotalPercentual.TabIndex = 54;
            this.lblTotalPercentual.Text = "0";
            // 
            // aaaa
            // 
            this.aaaa.AutoSize = true;
            this.aaaa.Location = new System.Drawing.Point(801, 606);
            this.aaaa.Name = "aaaa";
            this.aaaa.Size = new System.Drawing.Size(73, 17);
            this.aaaa.TabIndex = 53;
            this.aaaa.Text = "Percentual:";
            // 
            // GerenciaVendas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1482, 632);
            this.Controls.Add(this.lblTotalPercentual);
            this.Controls.Add(this.aaaa);
            this.Controls.Add(this.lblTotalTaxa);
            this.Controls.Add(this.bbb);
            this.Controls.Add(this.lblTotalLiquido);
            this.Controls.Add(this.ccc);
            this.Controls.Add(this.lblTotalBruto);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.lblQuantidadeRegistros);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dgvVendas);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Calibri", 10F);
            this.Name = "GerenciaVendas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gerência de Vendas";
            this.Load += new System.EventHandler(this.RelatorioVendas_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVendas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox txtQuantidadeParcelas;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtValorVendaFinal;
        private System.Windows.Forms.TextBox txtValorVendaInicial;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbOperacao;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbSituacao;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkDataCompensacao;
        private System.Windows.Forms.CheckBox chkDataTransacao;
        private System.Windows.Forms.DateTimePicker dtpDataCompensacaoFinal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpDataCompensacaoInicial;
        private System.Windows.Forms.ComboBox cmbTipoPagamento;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtpDataTransacaoFinal;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpDataTransacaoInicial;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.DataGridView dgvVendas;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblQuantidadeRegistros;
        private System.Windows.Forms.Label lblTotalBruto;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblTotalLiquido;
        private System.Windows.Forms.Label ccc;
        private System.Windows.Forms.Label lblTotalTaxa;
        private System.Windows.Forms.Label bbb;
        private System.Windows.Forms.Label lblTotalPercentual;
        private System.Windows.Forms.Label aaaa;
    }
}