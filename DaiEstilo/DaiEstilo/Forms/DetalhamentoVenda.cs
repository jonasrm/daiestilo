﻿using DaiEstilo.Classes;
using DaiEstilo.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DaiEstilo.Forms
{
    public partial class DetalhamentoVenda : Form
    {

        private int IdTransacao = 0;

        public DetalhamentoVenda()
        {
            InitializeComponent();
        }

        public DetalhamentoVenda(int idTransacao)
        {
            InitializeComponent();

            IdTransacao = idTransacao;
            CarregaTransacao(idTransacao);
            CarregaLojas();
            CarregaPecas();
            CarregaDetalhes();

            //default
            txtValor.Text = "R$ 0,00";
        }

        private void CarregaTransacao(int idVenda)
        {
            try
            {
                using (var db = new DaiEstiloEntities())
                {
                    dgvDetalhamentoVenda.DataSource = null;
                    dgvDetalhamentoVenda.DataSource = db.Transacoes.Include("TransacoesDetalhe").Where(q => q.IdTransacao == idVenda).ToList();
                    dgvDetalhamentoVenda.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Não foi possível carregar as informações da Venda (" + ex.Message + ").");
            }
        }

        private void DetalhamentoVenda_Load(object sender, EventArgs e)
        {
            Util.AplicarEventosMoeda(txtValor);
        }

        private void btnAdicionarPeca_Click(object sender, EventArgs e)
        {
            try
            {
                string retorno = Prompt.ShowDialog("Informe o nome da Peça que deseja adicionar:", "Adicionar Peça");
                retorno = retorno.Trim();

                if (string.IsNullOrEmpty(retorno))
                    return;

                using (var db = new DaiEstiloEntities())
                {
                    if (db.Pecas.Any(q => q.Peca.ToUpper() == retorno.ToUpper()))
                    {
                        MessageBox.Show("A peça informada já existe.");
                        return;
                    }
                    db.Pecas.Add(new Pecas()
                    {
                        Peca = retorno,
                        DataInclusao = DateTime.Now
                    });
                    db.SaveChanges();
                }
                CarregaPecas();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Não foi possível salvar (" + ex.Message + ").");
            }

        }

        private void btnAdicionarLoja_Click(object sender, EventArgs e)
        {
            try
            {
                string retorno = Prompt.ShowDialog("Informe o nome da Loja que deseja adicionar:", "Adicionar Loja");
                retorno = retorno.Trim();

                if (string.IsNullOrEmpty(retorno))
                    return;

                using (var db = new DaiEstiloEntities())
                {
                    if (db.Lojas.Any(q => q.Loja.ToUpper() == retorno.ToUpper()))
                    {
                        MessageBox.Show("A loja informada já existe.");
                        return;
                    }
                    db.Lojas.Add(new Lojas()
                    {
                        Loja = retorno,
                        DataInclusao = DateTime.Now
                    });
                    db.SaveChanges();
                }
                CarregaLojas();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Não foi possível salvar (" + ex.Message + ").");
            }
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                decimal Valor = 0;

                if (cmbLoja.Text == Util.ComboVazia)
                {
                    MessageBox.Show("Selecione a Loja.");
                    cmbLoja.Focus();
                    return;
                }

                if (cmbPeca.Text == Util.ComboVazia)
                {
                    MessageBox.Show("Selecione e Peça.");
                    cmbPeca.Focus();
                    return;
                }

                if (!string.IsNullOrEmpty(txtValor.Text))
                {
                    Valor = decimal.Parse(txtValor.Text.Replace("R$ ", ""));
                    if (Valor == 0)
                    {
                        MessageBox.Show("Informe o Valor.");
                        txtValor.Focus();
                    }
                }

                using (var db = new DaiEstiloEntities())
                {
                    db.TransacoesDetalhe.Add(new TransacoesDetalhe()
                    {
                        IdTransacao = this.IdTransacao,
                        Loja = cmbLoja.Text,
                        Peca = cmbPeca.Text,
                        Descricao = txtObservacao.Text,
                        Valor = Valor
                    });
                    db.SaveChanges();
                }

                CarregaDetalhes();

                cmbLoja.SelectedIndex = cmbLoja.Items.Count - 1;
                cmbPeca.SelectedIndex = cmbPeca.Items.Count - 1;
                txtValor.Text = "R$ 0,00";
                txtObservacao.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Não foi possível salvar (" + ex.Message + ").");
            }
        }

        private void CarregaLojas()
        {
            try
            {
                cmbLoja.DataSource = null;
                using (var db = new DaiEstiloEntities())
                {
                    var res = db.Lojas.Select(s => s.Loja).Distinct().ToList();
                    res.Add(Util.ComboVazia);
                    cmbLoja.DataSource = res;
                    cmbLoja.SelectedIndex = cmbLoja.Items.Count - 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Não foi possível carregar as informações das Lojas (" + ex.Message + ").");
            }
        }

        private void CarregaPecas()
        {
            try
            {
                cmbPeca.DataSource = null;
                using (var db = new DaiEstiloEntities())
                {
                    var res = db.Pecas.Select(s => s.Peca).Distinct().ToList();
                    res.Add(Util.ComboVazia);
                    cmbPeca.DataSource = res;
                    cmbPeca.SelectedIndex = cmbPeca.Items.Count - 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Não foi possível carregar as informações das Peças (" + ex.Message + ").");
            }
        }

        private void CarregaDetalhes()
        {
            try
            {
                using (var db = new DaiEstiloEntities())
                {
                    dgvDetalhes.DataSource = null;
                    dgvDetalhes.DataSource = db.TransacoesDetalhe
                        .Where(q => q.IdTransacao == this.IdTransacao)
                        .Select(s => new
                        {
                            Id = s.IdTransacaoDetalhe,
                            Loja = s.Loja,
                            Peça = s.Peca,
                            Valor = s.Valor
                        })
                        .ToList();
                    dgvDetalhes.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
                }

                decimal Total = 0;
                foreach (DataGridViewRow row in dgvDetalhes.Rows)
                {
                    Total += decimal.Parse(row.Cells["Valor"].Value.ToString());
                }
                lblTotal.Text = Total.ToString("c2");

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Não foi possível carregar as informações de Detalhes (" + ex.Message + ").");
            }
        }

        private void dgvDetalhes_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var hti = dgvDetalhes.HitTest(e.X, e.Y);
                dgvDetalhes.ClearSelection();
                dgvDetalhes.Rows[hti.RowIndex].Selected = true;
            }
        }

        private void DeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                var id = int.Parse(dgvDetalhes.Rows[dgvDetalhes.Rows.GetFirstRow(DataGridViewElementStates.Selected)].Cells["Id"].Value.ToString());
                using (var db = new DaiEstiloEntities())
                {
                    var tmp = db.TransacoesDetalhe.FirstOrDefault(q => q.IdTransacaoDetalhe == id);
                    db.TransacoesDetalhe.Remove(tmp);
                    db.SaveChanges();
                }
                CarregaDetalhes();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Não foi possível excluir (" + ex.Message + ").");
            }
        }
    }
}
