﻿using DaiEstilo.Classes;
using DaiEstilo.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DaiEstilo.Forms
{
    public partial class GerenciaVendas : Form
    {
        public GerenciaVendas()
        {
            InitializeComponent();
        }

        private void RelatorioVendas_Load(object sender, EventArgs e)
        {
            InicializaForm();
            CarregaTipoPagamento();
            CarregaSituacao();
            CarregaOperacao();
            Util.AplicarEventosMoeda(txtValorVendaInicial);
            Util.AplicarEventosMoeda(txtValorVendaFinal);
            Util.AplicarEventosInteiro(txtQuantidadeParcelas);
            CarregaValoresPadrao();
        }

        private void InicializaForm()
        {
            dtpDataTransacaoInicial.Enabled = false;
            dtpDataTransacaoFinal.Enabled = false;
            dtpDataCompensacaoInicial.Enabled = false;
            dtpDataCompensacaoFinal.Enabled = false;
        }

        private void CarregaTipoPagamento()
        {
            try
            {
                cmbTipoPagamento.DataSource = null;
                using (var db = new DaiEstiloEntities())
                {
                    var res = db.Transacoes.Select(s => s.Tipo_Transacao).Distinct().ToList();
                    res.Add(Util.ComboTodos);
                    cmbTipoPagamento.DataSource = res;
                    cmbTipoPagamento.SelectedIndex = cmbTipoPagamento.Items.Count - 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Não foi possível carregar as informações de Tipo de Pagamento  (" + ex.Message + ").");
            }
        }

        private void CarregaSituacao()
        {
            try
            {
                cmbSituacao.DataSource = null;
                using (var db = new DaiEstiloEntities())
                {
                    var res = db.Transacoes.Select(s => s.Status).Distinct().ToList();
                    res.Add(Util.ComboTodos);
                    cmbSituacao.DataSource = res;
                    cmbSituacao.SelectedIndex = cmbSituacao.Items.Count - 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Não foi possível carregar as informações da Situação (" + ex.Message + ").");
            }
        }

        private void CarregaOperacao()
        {
            try
            {
                cmbOperacao.DataSource = null;
                using (var db = new DaiEstiloEntities())
                {
                    var res = db.Transacoes.Select(s => s.Debito_Credito).Distinct().ToList();
                    res.Add(Util.ComboTodos);
                    cmbOperacao.DataSource = res;
                    cmbOperacao.SelectedIndex = cmbOperacao.Items.Count - 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Não foi possível carregar as informações de Operação (" + ex.Message + ").");
            }
        }

        private void CarregaValoresPadrao()
        {
            cmbSituacao.Text = "Aprovada";
            dtpDataTransacaoInicial.Value = DateTime.Now.AddDays(-5);
            dtpDataTransacaoFinal.Value = DateTime.Now;
            dtpDataCompensacaoInicial.Value = DateTime.Now.AddDays(-5);
            dtpDataCompensacaoInicial.Value = DateTime.Now;
            txtValorVendaInicial.Text = "R$ 0,00";
            txtValorVendaFinal.Text = "R$ 9.999,00";
        }

        private void btnPesquisa_Click(object sender, EventArgs e)
        {
            try
            {
                dgvVendas.DataSource = null;

                DateTime DataTransacaoFinal = Convert.ToDateTime(string.Concat(dtpDataTransacaoFinal.Value.ToShortDateString(), " 23:59:59"));
                DateTime DataCompensacaoFinal = Convert.ToDateTime(string.Concat(dtpDataCompensacaoFinal.Value.ToShortDateString(), " 23:59:59"));

                decimal TotalBruto = 0;
                decimal TotalLiquido = 0;
                decimal TotalTaxa = 0;
                decimal TotalPercentual = 0;

                using (var db = new DaiEstiloEntities())
                {
                    IQueryable<Transacoes> query = db.Transacoes.Include("TransacoesDetalhes");
                    if (chkDataTransacao.Checked)
                        query = query.Where(q => q.Data_Transacao >= dtpDataTransacaoInicial.Value.Date && q.Data_Transacao <= DataTransacaoFinal);

                    if (chkDataCompensacao.Checked)
                        query = query.Where(q => q.Data_Compensacao >= dtpDataCompensacaoInicial.Value.Date && q.Data_Transacao <= DataCompensacaoFinal);

                    if (cmbTipoPagamento.Text != Util.ComboTodos)
                        query = query.Where(q => q.Tipo_Pagamento == cmbTipoPagamento.Text);

                    if (cmbOperacao.Text != Util.ComboTodos)
                        query = query.Where(q => q.Debito_Credito == cmbOperacao.Text);

                    if (cmbSituacao.Text != Util.ComboTodos)
                        query = query.Where(q => q.Status == cmbSituacao.Text);

                    if (!string.IsNullOrEmpty(txtQuantidadeParcelas.Text))
                    {
                        int Valor = Int32.Parse(txtQuantidadeParcelas.Text);
                        query = query.Where(q => q.Parcelas.Value == Valor);
                    }

                    if (!string.IsNullOrEmpty(txtValorVendaInicial.Text))
                    {
                        decimal Valor = decimal.Parse(txtValorVendaInicial.Text.Replace("R$ ", ""));
                        query = query.Where(q => q.Valor_Bruto >= Valor);
                    }

                    if (!string.IsNullOrEmpty(txtValorVendaFinal.Text))
                    {
                        decimal Valor = decimal.Parse(txtValorVendaFinal.Text.Replace("R$ ", ""));
                        query = query.Where(q => q.Valor_Bruto <= Valor);
                    }

                    dgvVendas.DataSource = query.Select(s => new
                    {
                        ID = s.IdTransacao,
                        Situação = s.Status,
                        Operação = s.Debito_Credito,
                        Venda = s.Data_Transacao,
                        Compensação = s.Data_Compensacao,
                        Tipo = s.Tipo_Pagamento,
                        Parcelas = s.Parcelas,
                        Bruto = s.Valor_Bruto,
                        Líquido = s.Valor_Liquido,
                        TaxaTotal = s.Valor_Taxa,
                        Intermediação = s.Valor_Taxa_Intermediacao,
                        Parcelamento = s.Valor_Taxa_Parcelamento,
                        TaxaPercentual = ((s.Valor_Taxa * 100) / s.Valor_Bruto),
                        Custo = s.TransacoesDetalhe.Sum(sum => (decimal?)sum.Valor) ?? 0,
                        Lucro = (s.Valor_Liquido - (s.TransacoesDetalhe.Sum(sum => (decimal?)sum.Valor) ?? 0))
                    }).ToList();
                }

                //totalizações
                lblQuantidadeRegistros.Text = dgvVendas.RowCount.ToString();
                foreach (DataGridViewRow row in dgvVendas.Rows)
                {
                    TotalBruto += decimal.Parse(row.Cells["Bruto"].Value.ToString());
                    TotalLiquido += decimal.Parse(row.Cells["Líquido"].Value.ToString());
                    TotalTaxa += decimal.Parse(row.Cells["TaxaTotal"].Value.ToString());

                    if (decimal.Parse(row.Cells["Custo"].Value.ToString()) == 0)
                    {
                        row.DefaultCellStyle.BackColor = Color.Red;
                    }

                }

                if (TotalBruto > 0)
                    TotalPercentual = ((TotalTaxa * 100) / TotalBruto);

                lblTotalBruto.Text = TotalBruto.ToString("c2");
                lblTotalLiquido.Text = TotalLiquido.ToString("c2");
                lblTotalTaxa.Text = TotalTaxa.ToString("c2");
                lblTotalPercentual.Text = TotalPercentual.ToString("n2") + "%";

                dgvVendas.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Não foi possível realizar a consulta (" + ex.Message + ").");
            }
        }

        private void chkDataTransacao_CheckedChanged(object sender, EventArgs e)
        {
            dtpDataTransacaoInicial.Enabled = chkDataTransacao.Checked;
            dtpDataTransacaoFinal.Enabled = chkDataTransacao.Checked;
        }

        private void chkDataCompensacao_CheckedChanged(object sender, EventArgs e)
        {
            dtpDataCompensacaoInicial.Enabled = chkDataCompensacao.Checked;
            dtpDataCompensacaoFinal.Enabled = chkDataCompensacao.Checked;
        }

        private void dgvVendas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var row = dgvVendas.Rows[e.RowIndex];
            int id = Int32.Parse(row.Cells["ID"].Value.ToString());
            var frm = new DetalhamentoVenda(id);
            frm.ShowDialog(this);
        }
    }
}
