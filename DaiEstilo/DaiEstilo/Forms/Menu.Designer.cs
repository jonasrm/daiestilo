﻿namespace DaiEstilo.Forms
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnImportacaoArquivo = new System.Windows.Forms.Button();
            this.btnRelatorioVendas = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnImportacaoArquivo
            // 
            this.btnImportacaoArquivo.AutoSize = true;
            this.btnImportacaoArquivo.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnImportacaoArquivo.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnImportacaoArquivo.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportacaoArquivo.Location = new System.Drawing.Point(0, 0);
            this.btnImportacaoArquivo.Margin = new System.Windows.Forms.Padding(0);
            this.btnImportacaoArquivo.Name = "btnImportacaoArquivo";
            this.btnImportacaoArquivo.Padding = new System.Windows.Forms.Padding(23);
            this.btnImportacaoArquivo.Size = new System.Drawing.Size(436, 82);
            this.btnImportacaoArquivo.TabIndex = 0;
            this.btnImportacaoArquivo.Text = "Importação de Arquivo";
            this.btnImportacaoArquivo.UseVisualStyleBackColor = true;
            this.btnImportacaoArquivo.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnRelatorioVendas
            // 
            this.btnRelatorioVendas.AutoSize = true;
            this.btnRelatorioVendas.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnRelatorioVendas.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRelatorioVendas.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRelatorioVendas.Location = new System.Drawing.Point(0, 82);
            this.btnRelatorioVendas.Name = "btnRelatorioVendas";
            this.btnRelatorioVendas.Padding = new System.Windows.Forms.Padding(23);
            this.btnRelatorioVendas.Size = new System.Drawing.Size(436, 82);
            this.btnRelatorioVendas.TabIndex = 1;
            this.btnRelatorioVendas.Text = "Gerência de Vendas";
            this.btnRelatorioVendas.UseVisualStyleBackColor = true;
            this.btnRelatorioVendas.Click += new System.EventHandler(this.btnRelatorioVendas_Click);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(436, 466);
            this.Controls.Add(this.btnRelatorioVendas);
            this.Controls.Add(this.btnImportacaoArquivo);
            this.Font = new System.Drawing.Font("Calibri", 10F);
            this.IsMdiContainer = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnImportacaoArquivo;
        private System.Windows.Forms.Button btnRelatorioVendas;
    }
}