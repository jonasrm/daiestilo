﻿using DaiEstilo.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DaiEstilo.Forms
{
    public partial class ImportacaoArquivoTransacao : Form
    {
        public ImportacaoArquivoTransacao()
        {
            InitializeComponent();
        }

        private void btnInput_Click(object sender, EventArgs e)
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Informe o arquivo de Transações";
            fdlg.InitialDirectory = @"c:\";
            fdlg.Filter = "Arquivo de Texto (*.txt)|*.txt|Todos arquivos (*.*)|*.*";
            fdlg.FilterIndex = 2;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
                txtFile.Text = fdlg.FileName;
        }

        private void btnImportFile_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtFile.Text))
                {
                    MessageBox.Show("Por favor, informe um arquivo para importação!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                int currentLine = 0;
                int totalLine = 0;
                int totalImported = 0;

                var lines = File.ReadAllLines(txtFile.Text, Encoding.Default);
                totalLine = lines.Count();

                using (var db = new DaiEstiloEntities())
                {
                    foreach (var line in lines)
                    {
                        currentLine++;
                        progressBar1.Value = currentLine * 100 / totalLine;
                        var collumns = line.Split('\t');

                        if (currentLine == 1)
                            continue;

                        var id = collumns[0];

                        if (db.Transacoes.Any(q => q.Transacao_ID == id))
                            continue;

                        db.Transacoes.Add(new Transacoes()
                        {
                            Transacao_ID = id,
                            Cliente_Nome = collumns[1],
                            Cliente_Email = collumns[2],
                            Debito_Credito = collumns[3],
                            Tipo_Transacao = collumns[4],
                            Status = collumns[5],
                            Tipo_Pagamento = collumns[6],
                            Valor_Bruto = string.IsNullOrEmpty(collumns[7]) ? 0 : Decimal.Parse(collumns[7]),
                            Valor_Desconto = string.IsNullOrEmpty(collumns[8]) ? 0 : Decimal.Parse(collumns[8]),
                            Valor_Taxa = string.IsNullOrEmpty(collumns[9]) ? 0 : Decimal.Parse(collumns[9]),
                            Valor_Liquido = string.IsNullOrEmpty(collumns[10]) ? 0 : Decimal.Parse(collumns[10]),
                            Transportadora = collumns[11],
                            Num_Envio = collumns[12],
                            Data_Transacao = string.IsNullOrEmpty(collumns[13]) ? DateTime.Now : DateTime.Parse(collumns[13]),
                            Data_Compensacao = string.IsNullOrEmpty(collumns[14]) ? DateTime.Now : DateTime.Parse(collumns[14]),
                            Ref_Transacao = collumns[15],
                            Parcelas = string.IsNullOrEmpty(collumns[16]) ? 0 : Int32.Parse(collumns[16]),
                            Codigo_Usuario = string.IsNullOrEmpty(collumns[17]) ? 0 : Int32.Parse(collumns[17]),
                            Codigo_Venda = string.IsNullOrEmpty(collumns[18]) ? 0 : Int32.Parse(collumns[18]),
                            Serial_Leitor = string.IsNullOrEmpty(collumns[19]) ? 0 : Int32.Parse(collumns[19]),
                            Recebimentos = string.IsNullOrEmpty(collumns[20]) ? 0 : Int32.Parse(collumns[20]),
                            Recebidos = string.IsNullOrEmpty(collumns[21]) ? 0 : Int32.Parse(collumns[21]),
                            Valor_Recebido = string.IsNullOrEmpty(collumns[22]) ? 0 : Decimal.Parse(collumns[22]),
                            Valor_Tarifa_Intermediacao = string.IsNullOrEmpty(collumns[23]) ? 0 : Decimal.Parse(collumns[23]),
                            Valor_Taxa_Intermediacao = string.IsNullOrEmpty(collumns[24]) ? 0 : Decimal.Parse(collumns[24]),
                            Valor_Taxa_Parcelamento = string.IsNullOrEmpty(collumns[25]) ? 0 : Decimal.Parse(collumns[25]),
                            Valor_Tarifa_Boleto = string.IsNullOrEmpty(collumns[26]) ? 0 : Decimal.Parse(collumns[26])
                        });

                        totalImported++;

                    }

                    if (totalImported > 0)
                    {
                        db.SaveChanges();
                        MessageBox.Show("Importação realizada com sucesso (" + totalImported.ToString() + " registros)!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Nenhuma transação nova no arquivo informado.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }                   
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro na importação: " + ex.Message , "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
